package ir.vbile.app.todolist.ui.addedittask

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.todolist.R
import ir.vbile.app.todolist.databinding.FragmentAddEditTaskBinding
import ir.vbile.app.todolist.ui.AddEditTaskVM
import ir.vbile.app.todolist.util.exhaustive
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class AddEditTaskFragment : Fragment(R.layout.fragment_add_edit_task) {
    private val vm: AddEditTaskVM by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bindings = FragmentAddEditTaskBinding.bind(view)
        bindings.apply {
            editTextTaskName.setText(vm.taskName)
            checkBoxImportant.isChecked = vm.taskImportance
            checkBoxImportant.jumpDrawablesToCurrentState()
            textViewDateCreated.isVisible = vm.task != null
            textViewDateCreated.text = "Created: ${vm.task?.createdDateFormatted}"
            editTextTaskName.addTextChangedListener {
                vm.taskName = it.toString()
            }
            checkBoxImportant.setOnCheckedChangeListener { _, isChecked ->
                vm.taskImportance = isChecked
            }
            fabSaveTask.setOnClickListener {
                vm.onSaveClick()
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.addEditTaskEvent.collect { event ->
                when (event) {
                    is AddEditTaskVM.AddEditTaskEvent.ShowInvalidInputMessage -> {
                        Snackbar.make(requireView(), event.msg, Snackbar.LENGTH_SHORT).show()
                    }
                    is AddEditTaskVM.AddEditTaskEvent.NavigateBackWithResult -> {
                        bindings.editTextTaskName.clearFocus()
                        setFragmentResult(
                            "add_edit_request",
                            bundleOf("add_edit_result" to event.result)
                        )
                        findNavController().popBackStack()
                    }
                }.exhaustive
            }
        }
    }
}