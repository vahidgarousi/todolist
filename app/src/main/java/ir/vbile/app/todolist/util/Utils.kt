package ir.vbile.app.todolist.util

val <T> T.exhaustive: T
    get() = this